package at.mnm.cuttingcalculator;

import org.junit.Test;

public class CuttingStateBuilderTest {

   @Test(expected = IllegalArgumentException.class)
   public void shouldNotAllowNegativeSawBladeLoss() throws Exception {
      new CuttingStateBuilder().withStock(0);
   }

   @Test(expected = IllegalArgumentException.class)
   public void shouldNotAllowNegativeWorkpieces() throws Exception {
      new CuttingStateBuilder().withStock(0);
   }

   @Test(expected = IllegalArgumentException.class)
   public void shouldNotAllowNegativeExpectedSizes() throws Exception {
      new CuttingStateBuilder().withRequiredWorkPieces(1, 0);
   }

}
