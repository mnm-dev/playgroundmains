package at.mnm.cuttingcalculator;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Deque;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class CuttingState implements Comparable<CuttingState> {

   private static final long WEIGHT_LEFT_OVER_AVERAGE = 101;
   private static final long WEIGHT_LEFT_OVER_LONG_PIECE_MEDIAN = 100;
   public final Identity id;
   public final CuttingSettings cuttingSettings;
   private Long weightedSolution;

   private final Deque<Cut> allCuts;
   private final Deque<StockItemSplit> splitStockItems;

   private final IdObjectHashMap<StockItem> allStockItems;
   private final IdObjectHashMap<StockItem> usableStockItems;
   private final IdObjectHashMap<StockItem> usedUpStockItems;

   private final IdObjectHashMap<RequiredWorkPiece> allWorkPieces;
   private final IdObjectHashMap<RequiredWorkPiece> completedWorkPieces;
   private final IdObjectHashMap<RequiredWorkPiece> requiredWorkPieces;
   private final Map<Integer, List<Identity>> uniqueRequiredPieceLengths;

   public CuttingState(CuttingSettings cuttingSettings, IdObjectHashMap<StockItem> stockItems,
         IdObjectHashMap<RequiredWorkPiece> requiredWorkPieces) {
      this.id = new Identity();
      this.cuttingSettings = cuttingSettings;

      this.allCuts = new LinkedList<>();
      this.splitStockItems = new LinkedList<>();

      this.allStockItems = cloneAllStockItems(stockItems);
      this.allWorkPieces = cloneAllRequiredWorkPieces(requiredWorkPieces);

      this.usableStockItems = new IdObjectHashMap<>();
      this.usedUpStockItems = new IdObjectHashMap<>();
      this.requiredWorkPieces = new IdObjectHashMap<>();
      this.completedWorkPieces = new IdObjectHashMap<>();
      this.uniqueRequiredPieceLengths = new HashMap<>();

      for (StockItem stockItem : this.allStockItems.values()) {
         categorizeStockItem(stockItem);
      }
      for (RequiredWorkPiece requiredWorkPiece : this.allWorkPieces.values()) {
         categorizeRequiredWorkPiece(requiredWorkPiece);
      }
   }

   private CuttingState(CuttingState other) {
      this(other.cuttingSettings, other.allStockItems, other.allWorkPieces);

      this.weightedSolution = other.weightedSolution;

      for (Cut otherCut : other.allCuts) {
         this.allCuts.add(otherCut.clone());
      }

      for (StockItemSplit otherSplit : other.splitStockItems) {
         this.splitStockItems.add(otherSplit.clone());
      }
   }

   public CuttingState clone() {
      return new CuttingState(this);
   }

   public IdObjectHashMap<StockItem> cloneAllStockItems(IdObjectHashMap<StockItem> stockItems) {
      IdObjectHashMap<StockItem> allStockItemsMap = new IdObjectHashMap<>();
      for (StockItem entry : stockItems.values()) {
         StockItem clone = entry.clone();
         allStockItemsMap.put(clone);
      }
      return allStockItemsMap;
   }

   public IdObjectHashMap<RequiredWorkPiece> cloneAllRequiredWorkPieces(
         IdObjectHashMap<RequiredWorkPiece> requiredWorkPieces) {
      IdObjectHashMap<RequiredWorkPiece> allRequiredWorkPiecesMap = new IdObjectHashMap<>();
      for (RequiredWorkPiece entry : requiredWorkPieces.values()) {
         RequiredWorkPiece clone = entry.clone();
         allRequiredWorkPiecesMap.put(clone);
      }
      return allRequiredWorkPiecesMap;
   }

   private void categorizeStockItem(StockItem stockItem) {
      if (stockItem.isLargeEnough(cuttingSettings.minimumRequiredSize)) {
         usableStockItems.put(stockItem);
         usedUpStockItems.remove(stockItem);
      } else {
         usedUpStockItems.put(stockItem);
         usableStockItems.remove(stockItem);
      }
   }

   public List<StockItem> usableStockItems() {
      ArrayList<StockItem> arrayList = new ArrayList<StockItem>(usableStockItems.values());
      // it wont be empty, since we check before
      Collections.sort(arrayList, arrayList.get(0));
      return arrayList;
   }

   public List<Integer> uniqueRequiredPieceLengths() {
      return new ArrayList<Integer>(uniqueRequiredPieceLengths.keySet());
   }

   private void categorizeRequiredWorkPiece(RequiredWorkPiece requiredWorkPiece) {
      if (requiredWorkPiece.isComplete()) {
         completedWorkPieces.put(requiredWorkPiece);
         requiredWorkPieces.remove(requiredWorkPiece);
      } else {
         requiredWorkPieces.put(requiredWorkPiece);
         completedWorkPieces.remove(requiredWorkPiece);
         uniqueRequiredPieceLengths.computeIfAbsent(requiredWorkPiece.leftToCut, k -> new ArrayList<Identity>())
               .add(requiredWorkPiece.id);
      }
   }

   public void cleanPieces() {
      if (cuttingSettings.needCleanEnds) {
         for (StockItem stockItem : usableStockItems.values()) {
            stockItem.trimEnds(cuttingSettings.sawBladeLossLength);
         }
      }
   }

   public boolean outOfStock() {
      return usableStockItems.size() == 0;
   }

   public boolean solved() {
      return requiredWorkPieces.size() == 0;
   }

   public void cut(StockItem stockItem, RequiredWorkPiece requiredWorkPiece) {
      uniqueRequiredPieceLengths.get(requiredWorkPiece.leftToCut).remove(requiredWorkPiece.id);
      if (uniqueRequiredPieceLengths.get(requiredWorkPiece.leftToCut).isEmpty()) {
         uniqueRequiredPieceLengths.remove(requiredWorkPiece.leftToCut);
      }

      if (stockItem.isLargeEnough(requiredWorkPiece.leftToCut)) {
         reduceStockItemByRequiredWorkPieceDimension(requiredWorkPiece, stockItem);
      } else {
         useAllOfStockItemForRequiredPiece(requiredWorkPiece, stockItem);
      }

      categorizeStockItem(stockItem);
      categorizeRequiredWorkPiece(requiredWorkPiece);
   }

   public void revertLastCut() {
      Cut lastCut = allCuts.pop();

      StockItem stockItem = lastCut.stockItem();
      RequiredWorkPiece requiredWorkPiece = lastCut.requiredWorkPiece();

      if (requiredWorkPiece.leftToCut > 0) {
         uniqueRequiredPieceLengths.get(requiredWorkPiece.leftToCut).remove(requiredWorkPiece.id);
         if (uniqueRequiredPieceLengths.get(requiredWorkPiece.leftToCut).isEmpty()) {
            uniqueRequiredPieceLengths.remove(requiredWorkPiece.leftToCut);
         }
      }

      stockItem.revertLastCut();
      requiredWorkPiece.revertLastCut();

      categorizeStockItem(stockItem);
      categorizeRequiredWorkPiece(requiredWorkPiece);

   }

   private void reduceStockItemByRequiredWorkPieceDimension(RequiredWorkPiece requiredWorkPiece, StockItem stockItem) {
      stockItem.cut(requiredWorkPiece.leftToCut);
      requiredWorkPiece.cut(requiredWorkPiece.leftToCut);
      allCuts.push(new Cut(stockItem, requiredWorkPiece));
   }

   private void useAllOfStockItemForRequiredPiece(RequiredWorkPiece requiredWorkPiece, StockItem stockItem) {
      requiredWorkPiece.cut(stockItem.leftOver);
      stockItem.cut(stockItem.leftOver);
      allCuts.push(new Cut(stockItem, requiredWorkPiece));
   }

   public RequiredWorkPiece findItemWithLeftToCut(Integer leftToCut) {
      return allWorkPieces.get(uniqueRequiredPieceLengths.get(leftToCut).get(0));
   }

   public void splitStockIntoTwo(StockItem someStockItem) {

      int dimension = cuttingSettings.someAbritraryPiece / 2;
      someStockItem.cut(dimension);

      StockItem newSplitOfStockItem = new StockItem(dimension);
      allStockItems.put(newSplitOfStockItem);
      categorizeStockItem(newSplitOfStockItem);

      splitStockItems.push(new StockItemSplit(someStockItem, newSplitOfStockItem));
   }

   public void revertLastSplit() {

      StockItemSplit stockItemSplit = splitStockItems.pop();
      StockItem originalStockItem = stockItemSplit.someStockItem();
      StockItem newSplitOfStockItem = stockItemSplit.newSplitOffStockItem();

      originalStockItem.revertLastCut();

      allStockItems.remove(newSplitOfStockItem);
      usedUpStockItems.remove(newSplitOfStockItem);
      usableStockItems.remove(newSplitOfStockItem);
   }

   public void output(PrintStream out) {
      out.println("Solved : " + solved() + " weightedSolution : " + storedWeightedPreference() + " Instance : " + id);

      int cuts = 0;
      int sum = 0;
      int sumStock = 0;
      int sumCutOffs = 0;
      int pieces = 0;

      for (StockItem stockItem : allStockItems.values()) {
         cuts += stockItem.cutOffPieces.size();
         if (stockItem.leftOver > 0) {
            pieces++;
            sum += stockItem.leftOver;
         }
         sumStock += stockItem.originalDimension;
         sumCutOffs += stockItem.cutOffPieces.stream().mapToInt(Integer::intValue).sum();
      }
      String stockStuff = "original Stock: " + sumStock + " Cuts : " + cuts + "  average left over : " + (sum / pieces)
            + "  pieces : " + allCuts.size() + "  sumLeftOver : " + sum + "  sumCutOffs : " + sumCutOffs;
      out.println(stockStuff);
      out.println("usableStockItems : ");
      for (StockItem stockItem : allStockItems.values()) {
         stockItem.output(out);
      }

      out.println("requiredWorkPieces : ");
      sum = 0;
      sumCutOffs = 0;
      pieces = 0;
      for (RequiredWorkPiece requiredWorkPiece : allWorkPieces.values()) {
         requiredWorkPiece.output(out);
         cuts += requiredWorkPiece.parts.size();
         sumCutOffs += requiredWorkPiece.parts.stream().mapToInt(Integer::intValue).sum();
         sum += requiredWorkPiece.requiredDimension;
      }
      out.println("    original : " + sum + " pieces : " + cuts + " / sum pieces : " + sumCutOffs);

   }

   public StockItem findRandomUsableStockItemFor(int someAbritraryPiece) {
      Random random = new Random();
      for (StockItem stockItem : usableStockItems.values()) {
         if (stockItem.leftOver > someAbritraryPiece) {
            if (random.nextBoolean()) {
               return stockItem;
            }
         }
      }
      return null;
   }

   public long calculateWeightedSolution() {

      List<Integer> relevantLengths = new ArrayList<>(usedUpStockItems.values().size() + usableStockItems.size());

      int countPieces = 0;
      int sumLeftOver = 0;

      for (StockItem stockItem : allStockItems.values()) {
         if (stockItem.leftOver > 220) {
            relevantLengths.add(stockItem.leftOver);
         }
         countPieces++;
         sumLeftOver += stockItem.leftOver;
      }

      if (countPieces == 0) {
         return 0;
      }

      Collections.sort(relevantLengths);
      // semi median:
      long longPiecesValue = relevantLengths.get(relevantLengths.size() / 2);
      if (relevantLengths.size() % 2 == 0) {
         longPiecesValue = (longPiecesValue + relevantLengths.get(relevantLengths.size() / 2 - 1)) / 2;
      }

      long averagePiceSize = sumLeftOver / countPieces;

      return longPiecesValue * WEIGHT_LEFT_OVER_LONG_PIECE_MEDIAN + averagePiceSize * WEIGHT_LEFT_OVER_AVERAGE;

   }

   public long storedWeightedPreference() {
      if (weightedSolution == null) {
         weightedSolution = calculateWeightedSolution();
      }
      return weightedSolution;
   }

   public String baseStats() {
      return "usedUpStockItems: " + usedUpStockItems.size()
            + " usableStockItems:" + usableStockItems.size()
            + " completedWorkPieces:" + completedWorkPieces.size()
            + " requiredWorkPieces:" + requiredWorkPieces.size();
   }

   @Override
   public String toString() {
      return super.toString() + " [solved=" + solved() + ", id=" + id + ", allStockItems=" + allStockItems.values()
            + ", allWorkPieces=" + allWorkPieces.values() + ", allCuts=" + allCuts + "]";
   }

   @Override
   public int compareTo(CuttingState other) {

      if (this.solved() && !other.solved()) {
         return -1;
      }
      if (!this.solved() && other.solved()) {
         return 1;
      }

      return (int) (other.storedWeightedPreference() - this.storedWeightedPreference());
   }

   @Override
   public int hashCode() {
      final int prime = 31;
      int result = 1;
      result = prime * result + ((allCuts == null) ? 0 : allCuts.hashCode());
      result = prime * result + ((requiredWorkPieces == null) ? 0 : requiredWorkPieces.hashCode());
      result = prime * result + ((usableStockItems == null) ? 0 : usableStockItems.hashCode());
      result = prime * result + ((weightedSolution == null) ? 0 : weightedSolution.hashCode());
      return result;
   }

   @Override
   public boolean equals(Object obj) {
      if (this == obj)
         return true;
      if (obj == null)
         return false;
      if (getClass() != obj.getClass())
         return false;
      CuttingState other = (CuttingState) obj;
      if (allCuts == null) {
         if (other.allCuts != null)
            return false;
      } else if (!allCuts.equals(other.allCuts))
         return false;
      if (requiredWorkPieces == null) {
         if (other.requiredWorkPieces != null)
            return false;
      } else if (!requiredWorkPieces.equals(other.requiredWorkPieces))
         return false;
      if (usableStockItems == null) {
         if (other.usableStockItems != null)
            return false;
      } else if (!usableStockItems.equals(other.usableStockItems))
         return false;
      if (weightedSolution == null) {
         if (other.weightedSolution != null)
            return false;
      } else if (!weightedSolution.equals(other.weightedSolution))
         return false;
      return true;
   }

}