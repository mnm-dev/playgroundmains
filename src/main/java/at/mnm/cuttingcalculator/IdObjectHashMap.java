package at.mnm.cuttingcalculator;

import java.util.TreeMap;

public class IdObjectHashMap<T extends HasIdentity<T>> extends TreeMap<Identity, T> {

   private static final long serialVersionUID = -7524821036822795110L;

   public HasIdentity<T> put(T value) {
      return super.put(value.getId(), value);
   }

   public void remove(T value) {
      super.remove(value.getId());
   }

   @Override
   public IdObjectHashMap<T> clone() {
      IdObjectHashMap<T> allT = new IdObjectHashMap<>();
      for (T entry : allT.values()) {
         T clone = entry.clone();
         allT.put(clone);
      }
      return allT;
   }
}
