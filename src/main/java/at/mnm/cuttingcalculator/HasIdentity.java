package at.mnm.cuttingcalculator;

public interface HasIdentity<T> {
   Identity getId();

   T clone();
}
