package at.mnm.cuttingcalculator;

public class StockItemSplit {

   public final Identity id;
   private final StockItem someStockItem;
   private final StockItem newSplitOffStockItem;

   public StockItemSplit(StockItem someStockItem, StockItem newSplitOffStockItem) {
      this(new Identity(), someStockItem, newSplitOffStockItem);
   }

   private StockItemSplit(Identity id, StockItem someStockItem, StockItem newSplitOffStockItem) {
      this.id = id;
      this.someStockItem = someStockItem;
      this.newSplitOffStockItem = newSplitOffStockItem;
   }

   public StockItemSplit clone() {
      return new StockItemSplit(id, someStockItem.clone(), newSplitOffStockItem.clone());
   }

   public StockItem someStockItem() {
      return someStockItem;
   }

   public StockItem newSplitOffStockItem() {
      return newSplitOffStockItem;
   }

   @Override
   public String toString() {
      return super.toString() + " [id=" + id + ", someStockItem=" + someStockItem + ", newSplitOffStockItem="
            + newSplitOffStockItem + "]";
   }

   @Override
   public int hashCode() {
      final int prime = 31;
      int result = 1;
      result = prime * result + ((id == null) ? 0 : id.hashCode());
      result = prime * result + ((newSplitOffStockItem == null) ? 0 : newSplitOffStockItem.hashCode());
      result = prime * result + ((someStockItem == null) ? 0 : someStockItem.hashCode());
      return result;
   }

   @Override
   public boolean equals(Object obj) {
      if (this == obj)
         return true;
      if (obj == null)
         return false;
      if (getClass() != obj.getClass())
         return false;
      StockItemSplit other = (StockItemSplit) obj;
      if (id == null) {
         if (other.id != null)
            return false;
      } else if (!id.equals(other.id))
         return false;
      if (newSplitOffStockItem == null) {
         if (other.newSplitOffStockItem != null)
            return false;
      } else if (!newSplitOffStockItem.equals(other.newSplitOffStockItem))
         return false;
      if (someStockItem == null) {
         if (other.someStockItem != null)
            return false;
      } else if (!someStockItem.equals(other.someStockItem))
         return false;
      return true;
   }

}
