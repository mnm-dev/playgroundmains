package at.mnm.cuttingcalculator;

import java.util.concurrent.atomic.AtomicInteger;

public class Identity implements Comparable<Identity> {

   public static final AtomicInteger objectCounter = new AtomicInteger(1);
   private final int id;

   public Identity() {
      this.id = objectCounter.getAndIncrement();
   }

   public Identity(int id) {
      this.id = id;
   }

   public int getId() {
      return id;
   }

   @Override
   public String toString() {
      return Integer.toString(id);
   }

   @Override
   public int hashCode() {
      return id;
   }

   @Override
   public boolean equals(Object obj) {
      if (this == obj)
         return true;
      if (obj == null)
         return false;
      if (getClass() != obj.getClass())
         return false;
      Identity other = (Identity) obj;
      if (id != other.id)
         return false;
      return true;
   }

   @Override
   public int compareTo(Identity other) {
      return this.id - other.id;
   }
}
