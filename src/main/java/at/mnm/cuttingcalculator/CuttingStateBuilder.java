package at.mnm.cuttingcalculator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class CuttingStateBuilder {

   private List<Integer> stockItemsSizes = new ArrayList<>();
   private List<Integer> requiredWorkPieceSizes = new ArrayList<>();

   private CuttingSettings cuttingSettings;

   public CuttingStateBuilder withSettings(CuttingSettings cuttingSettings) {
      this.cuttingSettings = cuttingSettings;
      return this;
   }

   public CuttingStateBuilder withStock(Integer... pieces) {
      stockItemsSizes.addAll(Arrays.asList(pieces));
      return this;
   }

   public CuttingStateBuilder withRequiredWorkPieces(int count, Integer size) {
      if (count <= 0 || size <= 0) {
         throw new IllegalArgumentException("Expected sizes and counts have to be positive numbers");
      }
      for (int itemNr = 0; itemNr < count; itemNr++) {
         requiredWorkPieceSizes.add(size);
      }
      return this;
   }

   public CuttingState build() {

      IdObjectHashMap<StockItem> allStockItems = new IdObjectHashMap<>();
      IdObjectHashMap<RequiredWorkPiece> allRequiredWorkPieces = new IdObjectHashMap<>();

      Collections.sort(stockItemsSizes);
      for (Integer dimension : stockItemsSizes) {
         StockItem stockItem = new StockItem(dimension);
         allStockItems.put(stockItem);
      }

      Collections.sort(requiredWorkPieceSizes);
      Collections.reverse(requiredWorkPieceSizes);
      for (Integer dimension : requiredWorkPieceSizes) {
         allRequiredWorkPieces.put(new RequiredWorkPiece(dimension));

      }

      return new CuttingState(cuttingSettings, allStockItems, allRequiredWorkPieces);
   }
}
