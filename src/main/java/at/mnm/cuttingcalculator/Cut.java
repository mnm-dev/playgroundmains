package at.mnm.cuttingcalculator;

public class Cut {

   public final Identity id;
   private final RequiredWorkPiece requiredWorkPiece;
   private final StockItem stockItem;

   public Cut(StockItem stockItem, RequiredWorkPiece requiredWorkPiece) {
      this(new Identity(), stockItem, requiredWorkPiece);
   }

   private Cut(Identity id, StockItem stockItem, RequiredWorkPiece requiredWorkPiece) {
      this.id = id;
      this.stockItem = stockItem;
      this.requiredWorkPiece = requiredWorkPiece;
   }

   public Cut clone() {
      return new Cut(id, stockItem.clone(), requiredWorkPiece.clone());
   }

   public RequiredWorkPiece requiredWorkPiece() {
      return requiredWorkPiece;
   }

   public StockItem stockItem() {
      return stockItem;
   }

   @Override
   public String toString() {
      return super.toString() + " [id=" + id + ", requiredWorkPiece=" + requiredWorkPiece + ", stockItem=" + stockItem
            + "]";
   }

   @Override
   public int hashCode() {
      final int prime = 31;
      int result = 1;
      result = prime * result + ((requiredWorkPiece == null) ? 0 : requiredWorkPiece.hashCode());
      result = prime * result + ((stockItem == null) ? 0 : stockItem.hashCode());
      return result;
   }

   @Override
   public boolean equals(Object obj) {
      if (this == obj)
         return true;
      if (obj == null)
         return false;
      if (getClass() != obj.getClass())
         return false;
      Cut other = (Cut) obj;
      if (requiredWorkPiece == null) {
         if (other.requiredWorkPiece != null)
            return false;
      } else if (!requiredWorkPiece.equals(other.requiredWorkPiece))
         return false;
      if (stockItem == null) {
         if (other.stockItem != null)
            return false;
      } else if (!stockItem.equals(other.stockItem))
         return false;
      return true;
   }

}
