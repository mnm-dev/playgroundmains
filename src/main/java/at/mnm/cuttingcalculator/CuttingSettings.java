package at.mnm.cuttingcalculator;

public class CuttingSettings {

   /**
    * when we don't have any matching pieces, we take one of the required Pieces
    * and cut off SOME_CUTOFF_FACTOR times the minimumRequiredSize and we expect
    * the piece to be at least twice that size (> SOME_CUTOFF_FACTOR * , *
    * minimumRequiredSize)
    */
   public static final double SOME_CUTOFF_FACTOR = 1.5;
   public static final boolean SAFE_MODE = true;

   public final int minimumRequiredSize;
   public final int sawBladeLossLength;
   public final int maxSectionsPerRequiredSize;
   public final boolean needCleanEnds;
   public final int someAbritraryPiece;
   public final int safetyMargin;
   public final boolean allowArbitraryCuts;

   public CuttingSettings(
         int minimumRequiredSize, int sawBladeLossLength, int maxSectionsPerRequiredSize,
         boolean needCleanEnds, int someAbritraryPiece, int safetyMargin

   ) {
      this.minimumRequiredSize = minimumRequiredSize;
      this.sawBladeLossLength = sawBladeLossLength;
      this.maxSectionsPerRequiredSize = maxSectionsPerRequiredSize;
      this.needCleanEnds = needCleanEnds;
      this.someAbritraryPiece = someAbritraryPiece;
      if (someAbritraryPiece > 0) {
         this.allowArbitraryCuts = true;
      } else {
         this.allowArbitraryCuts = false;
      }

      this.safetyMargin = safetyMargin;
   }

   @Override
   public String toString() {
      return super.toString() + " [minimumRequiredSize=" + minimumRequiredSize + ", sawBladeLossLength="
            + sawBladeLossLength + ", maxSectionsPerRequiredSize=" + maxSectionsPerRequiredSize + ", needCleanEnds="
            + needCleanEnds + ", someAbritraryPiece=" + someAbritraryPiece + ", safetyMargin=" + safetyMargin
            + "]";
   }

}
