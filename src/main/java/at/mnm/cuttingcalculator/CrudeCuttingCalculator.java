package at.mnm.cuttingcalculator;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

import org.apache.commons.lang3.time.StopWatch;

public class CrudeCuttingCalculator implements Runnable {

   private static final long MAX_RECURSION_CALLS = 500_000_000L;
   private static final int MAX_SOLUTIONS = 100000;
   private static final int MAX_SOLUTIONS_TO_REPORT = 400;

   private AtomicLong currentCuttingAttemptCount;
   private AtomicLong numberOfArbitraryCuts;
   private int depth;
   private int tmpMinDepth;
   private int tmpMaxDepth;
   private TreeSet<CuttingState> allCompletedCuttingStates;
   private Set<Long> seenSolutions;
   private CuttingState currentCuttingState;
   RunState runState;
   private int outOfStock;
   private PrintStream output;
   private StopWatch stopWatch;
   private long maxCuttingAttempts;
   private ThreadPoolExecutor threadPool;
   private CuttingSettings cuttingSettings;

   public CrudeCuttingCalculator() {

   }

   private CrudeCuttingCalculator setup(CuttingSettings cuttingSettings, CuttingState cuttingState,
         long maxCuttingAttempts, PrintStream output,
         ExecutorService executorService) {

      this.cuttingSettings = cuttingSettings;
      this.currentCuttingState = cuttingState;
      this.threadPool = (ThreadPoolExecutor) executorService;
      this.seenSolutions = new HashSet<>();
      this.allCompletedCuttingStates = new TreeSet<>();

      this.maxCuttingAttempts = maxCuttingAttempts;
      this.currentCuttingAttemptCount = new AtomicLong(0);
      this.numberOfArbitraryCuts = new AtomicLong(0);
      this.output = output;
      runState = new RunState();
      stopWatch = null;
      outOfStock = 0;
      depth = 0;
      return this;
   }

   private CrudeCuttingCalculator(CrudeCuttingCalculator other) {
      this.allCompletedCuttingStates = other.allCompletedCuttingStates;
      this.numberOfArbitraryCuts = other.numberOfArbitraryCuts;
      this.seenSolutions = other.seenSolutions;
      this.currentCuttingState = other.currentCuttingState.clone();
      this.threadPool = other.threadPool;
      this.cuttingSettings = other.cuttingSettings;

      this.currentCuttingAttemptCount = other.currentCuttingAttemptCount;
      this.depth = other.depth;
      this.tmpMinDepth = other.tmpMinDepth;
      this.tmpMaxDepth = other.tmpMaxDepth;
      this.runState = other.runState;
      this.outOfStock = other.outOfStock;
      this.output = other.output;
      this.maxCuttingAttempts = other.maxCuttingAttempts;
   }

   public CrudeCuttingCalculator clone() {
      return new CrudeCuttingCalculator(this);
   }

   public CrudeCuttingCalculator bruteForceIt() throws InterruptedException {
      if (hasNotBeenInitializedEver() || hasNotBeenSetupAgainAfterLastRun()) {
         throw new IllegalStateException("setup() has to be called before a new run ");
      }

      Runtime.getRuntime().addShutdownHook(new Thread() {
         public void run() {
            System.out.println("shutdown hook  captured  -> quitting gracefully (max 60 seconds wait)");
            output.println("shutdown hook  captured  -> quitting gracefully (max 60 seconds wait)");
            runState.quitting.set(true);
            output.flush();

            LocalDateTime maxWaitTime = LocalDateTime.now().plusSeconds(60);
            while (!runState.complete.get() && LocalDateTime.now().isBefore(maxWaitTime)) {
               try {
                  Thread.sleep(250);
                  System.out.print(".");
               } catch (InterruptedException e) {
                  e.printStackTrace();
               }
            }
            System.out.println();
         }
      });

      currentCuttingState.cleanPieces();

      startProcessing();

      return this;
   }

   private void startProcessing() throws InterruptedException {
      stopWatch = new StopWatch();

      stopWatch.start();
      trialPartialPieces();
      stopWatch.stop();

   }

   private boolean hasNotBeenSetupAgainAfterLastRun() {
      return stopWatch != null;
   }

   private boolean hasNotBeenInitializedEver() {
      return (currentCuttingState == null);
   }

   private void trialPartialPieces() {

      try {
         depth++;

         if (stopProcessing()) {
            return;
         }

         boolean noUsableStockItem = true;

         List<Integer> values = (currentCuttingState.uniqueRequiredPieceLengths());

         List<StockItem> usableStockItems = currentCuttingState.usableStockItems();

         List<Callable<Object>> tasks = new ArrayList<>();

         for (int stockNumber = 0; stockNumber < usableStockItems.size(); stockNumber++) {
            StockItem stockItem = usableStockItems.get(stockNumber);

            for (Integer leftToCut : values) {
               if (stockItem.isLargeEnough(leftToCut)
                     || ((leftToCut - stockItem.leftOver) > currentCuttingState.cuttingSettings.minimumRequiredSize)) {

                  noUsableStockItem = false;
                  RequiredWorkPiece requiredWorkPiece = currentCuttingState.findItemWithLeftToCut(leftToCut);
                  currentCuttingState.cut(stockItem, requiredWorkPiece);

                  if (runMultiThreaded()) {
                     tasks.add(Executors.callable(clone()));
                  } else {
                     trialPartialPieces();
                  }

                  currentCuttingState.revertLastCut();
               }
            }
         }

         if (runMultiThreaded()) {
            try {
               threadPool.invokeAll(tasks);
            } catch (InterruptedException e) {
               output.println("Thread was interupted 2.");
               runState.quitting.set(true);
            }
         }

         if (cuttingSettings.allowArbitraryCuts && noUsableStockItem) {

            StockItem eligibleStockItem =
                  currentCuttingState
                        .findRandomUsableStockItemFor(currentCuttingState.cuttingSettings.someAbritraryPiece + 1);
            if (eligibleStockItem == null) {
               // can't continue with this current cutting state
               return;
            }

            numberOfArbitraryCuts.incrementAndGet();

            currentCuttingState.splitStockIntoTwo(eligibleStockItem);

            // just try with these changes
            trialPartialPieces();

            currentCuttingState.revertLastSplit();
         }
      } catch (Exception e) {
         runState.quitting.set(true);
         e.printStackTrace();
      } finally {
         depth--;
      }
   }

   private boolean runMultiThreaded() {
      return depth == 1 && threadPool.getMaximumPoolSize() > 1;
   }

   @Override
   public void run() {
      try {
         startProcessing();
      } catch (InterruptedException e) {
         output.println("Thread was interupted 3.");
         runState.quitting.set(true);
      }
   }

   private boolean stopProcessing() {

      if (!runState.quitting.get()) {
         if (currentCuttingState.outOfStock()) {
            outOfStock++;
            return true;
         }

         if (currentCuttingState.solved()) {
            addCurrentCuttingStateToFinalResults();
            return true;
         }

         if (allCompletedCuttingStates.size() > MAX_SOLUTIONS) {
            output.println("Found " + MAX_SOLUTIONS + " solutions: stopping");
            runState.quitting.set(true);
         }

         if (currentCuttingAttemptCount.incrementAndGet() >= maxCuttingAttempts) {
            output.println("Reached maxTrials -> quitting with state:");
            runState.quitting.set(true);
         }

         if (Thread.currentThread().isInterrupted()) {
            output.println("Thread was interupted 1.");
            runState.quitting.set(true);
         }

         tmpMinDepth = Math.min(tmpMinDepth, depth);
         tmpMaxDepth = Math.max(tmpMaxDepth, depth);

         if (currentCuttingAttemptCount.get() % (maxCuttingAttempts / 10) == 0) {
            stopWatch.split();
            output.println(LocalDateTime.now().toString()
                  + " - " + Thread.currentThread().getName()
                  + " depth: " + depth
                  + " currentTrialCount: " + currentCuttingAttemptCount.get() + "/" + maxCuttingAttempts
                  + " solutions: " + allCompletedCuttingStates.size() + "  time spent: "
                  + (stopWatch.getSplitTime() / 1000) + " seconds "
                  + " tmpMinDepth: " + tmpMinDepth + " tmpMaxDepth: " + tmpMaxDepth
                  + " cuttingState: " + currentCuttingState.baseStats());
            output.flush();
            tmpMinDepth = -1;
            tmpMinDepth = Integer.MAX_VALUE;
            stopWatch.unsplit();
         }
      }
      if (depth < 2) {
         output.println(LocalDateTime.now().toString()
               + " - " + Thread.currentThread().getName()
               + " depth: " + depth
               + " currentTrialCount: " + currentCuttingAttemptCount.get() + "/" + maxCuttingAttempts
               + " solutions: " + allCompletedCuttingStates.size()
               + " cuttingState: " + currentCuttingState.baseStats());
      }

      return runState.quitting.get();
   }

   private void addCurrentCuttingStateToFinalResults() {
      // never use storedWeightedPreference, because it will store it in the currentCuttingState
      // and then will never recalculate it again
      synchronized (seenSolutions) {
         Long calculateWeightedSolution = currentCuttingState.calculateWeightedSolution();
         if (seenSolutions.contains(calculateWeightedSolution)) {
            return;
         }
         seenSolutions.add(calculateWeightedSolution);
         
         CuttingState clone = currentCuttingState.clone();
         allCompletedCuttingStates.add(clone);
      }
   }

   private void outputTrials() {

      output.println("Processing time: " + stopWatch.getTime() / 1000 + " seconds");
      output.println("Ran for " + maxCuttingAttempts + " left : " + currentCuttingAttemptCount.get());
      output.println("Ran out of stock : " + outOfStock + " times, had " + numberOfArbitraryCuts + " arbitrary cuts");

      try {
         if (allCompletedCuttingStates.isEmpty()) {
            output.println("no solution found.");
         } else {
            output.println("Results : " + allCompletedCuttingStates.size());
            int reportedStates = 0;
            for (CuttingState cuttingState : allCompletedCuttingStates) {
               if (reportedStates++ > MAX_SOLUTIONS_TO_REPORT) {
                  return;
               }
               output.println("-----");
               cuttingState.output(output);
            }
         }
         output.flush();
      } finally {
         
         runState.complete.set(true);
      }
   }

   // @formatter:off
   public static void main(String[] args) throws FileNotFoundException, InterruptedException {

      CuttingSettings cuttingSettings = 
            new CuttingSettingsBuilder()
               .withMinimumRequiredSize(460)
               .withCleanEnds()
               .withSawBladeLossLength(5)
               .maxSectionsPerRequiredLength(3)
               .withSafetyMargin(20)
               .withoutArbitraryCuts()
               .build();

         CuttingState cuttingState = new CuttingStateBuilder()
               // dark (12 pieces)
               .withStock(
                     new Integer[] { 
                           1971, 1962, 1852, 1785, 1557, 1477, 1507, 1393, 1492, 1544, 1460, 1354
                     }
               )
//               // light brown (20 pieces)
//               .withStock(
//                     new Integer[] {
//                           2443, 2405, 1885, 1810, 2031, 1711, 1948,
//                           1486, 1778, 1650, 1700, 1710, 1523, 1582, 1697, 1614, 1472,
//                           1430, 1348, 1319
//                     }
//               )
//               // problematic (2 pieces)
//               .withStock(
//                     new Integer[] {
//                           2203, 1875
//                     }
//                     
//               )
               .withRequiredWorkPieces(5, 1230*2)
               .withRequiredWorkPieces(2, 1230)
               
//               .withRequiredWorkPieces(5, 2600)
//               .withRequiredWorkPieces(2, 1300)
   //          .withRequiredWorkPieces(15,  900)
   //          .withRequiredWorkPieces(10, 2600)
   //          .withRequiredWorkPieces(4, 1300)
               .withSettings(cuttingSettings)
               .build();
         
         ThreadFactory threadFactory = new ThreadFactory() {
            private final AtomicInteger threadNumber = new AtomicInteger(1);
            public Thread newThread(Runnable r) {
                return new Thread(r, "cut-" + threadNumber.getAndIncrement());
            }
         };
         
         long maxTrials = MAX_RECURSION_CALLS;
         File outputFile = null;
         if (args.length > 0) {
            maxTrials = Long.parseLong(args[0]);
            outputFile =
                  new File(".", CrudeCuttingCalculator.class.getSimpleName() + "_" + maxTrials + "_"
                        + LocalDateTime.now().toString());
         }
         
         try (PrintStream output = getPrintStream(outputFile)) {
            ExecutorService threadPool = Executors.newFixedThreadPool(5, threadFactory);
            new CrudeCuttingCalculator()
                  .setup(cuttingSettings, cuttingState, maxTrials, output, threadPool)
                  .bruteForceIt()
                  .outputTrials();
            System.out.println("Shutting down");
            threadPool.shutdown();
            threadPool.awaitTermination(15, TimeUnit.SECONDS);

         } catch (Exception e) {
         }
         
   }
   // @formatter:on

   private static PrintStream getPrintStream(File outputFile) throws FileNotFoundException {
      if (outputFile != null) {
         return new PrintStream(outputFile);
      } else {
         return System.out;
      }
   }
}
