package at.mnm.cuttingcalculator;

import java.io.PrintStream;
import java.util.Comparator;
import java.util.Deque;
import java.util.LinkedList;

public class StockItem implements HasIdentity<StockItem>, Comparable<StockItem>, Comparator<StockItem> {

   public final Identity id;
   public final int originalDimension;
   public int leftOver;
   public Deque<Integer> cutOffPieces;

   public StockItem(Integer dimension) {
      if (dimension <= 0) {
         throw new IllegalArgumentException("Dimensions have to be positive");
      }
      this.id = new Identity();
      this.originalDimension = dimension;
      this.leftOver = dimension;
      this.cutOffPieces = new LinkedList<>();
   }

   private StockItem(Identity id, Integer dimension, int leftOver, Deque<Integer> cutOffPieces) {
      this.id = id;
      originalDimension = dimension;
      this.leftOver = leftOver;
      this.cutOffPieces = new LinkedList<>(cutOffPieces);
   }

   public StockItem clone() {
      return new StockItem(id, originalDimension, this.leftOver, this.cutOffPieces);
   }

   public void cut(Integer cutDimension) {
      if (CuttingSettings.SAFE_MODE && (cutDimension > leftOver || cutDimension <= 0)) {
         throw new IllegalStateException("Bogus cut: cutDimension: " + cutDimension + " originalDimension: "
               + originalDimension + " leftOver: " + leftOver);
      }
      cutOffPieces.push(cutDimension);
      leftOver -= cutDimension;
   }

   public boolean isLargeEnough(Integer size) {
      return size <= leftOver;
   }

   public void trimEnds(int sawBladeLossLength) {
      leftOver = originalDimension - 2 * sawBladeLossLength;
   }

   public void revertLastCut() {
      Integer cutDimension = cutOffPieces.pop();

      if (CuttingSettings.SAFE_MODE && ((cutDimension + leftOver) > originalDimension || cutDimension <= 0)) {
         throw new IllegalStateException("Bogus revert: cutDimension: " + cutDimension + " originalDimension: "
               + originalDimension + " leftOver: " + leftOver);
      }

      leftOver += cutDimension;
   }

   public void output(PrintStream out) {
      out.println("    originalDimension : " + originalDimension + ", leftOver : " + leftOver + ", cutOffPieces : "
            + cutOffPieces);
   }

   @Override
   public Identity getId() {
      return id;
   }

   @Override
   public int compareTo(StockItem other) {
      return compare(this, other);
   }

   @Override
   public int compare(StockItem o1, StockItem o2) {
      return o1.leftOver - o2.leftOver;
   }

   @Override
   public String toString() {
      return super.toString() + " [id=" + id + ", originalDimension=" + originalDimension + ", leftOver=" + leftOver
            + ", cutOffPieces=" + cutOffPieces + "]";
   }

   @Override
   public int hashCode() {
      final int prime = 31;
      int result = 1;
      result = prime * result + ((cutOffPieces == null) ? 0 : cutOffPieces.hashCode());
      result = prime * result + leftOver;
      result = prime * result + originalDimension;
      return result;
   }

   @Override
   public boolean equals(Object obj) {
      if (this == obj)
         return true;
      if (obj == null)
         return false;
      if (getClass() != obj.getClass())
         return false;
      StockItem other = (StockItem) obj;
      if (cutOffPieces == null) {
         if (other.cutOffPieces != null)
            return false;
      } else if (!cutOffPieces.equals(other.cutOffPieces))
         return false;
      if (leftOver != other.leftOver)
         return false;
      if (originalDimension != other.originalDimension)
         return false;
      return true;
   }

}
