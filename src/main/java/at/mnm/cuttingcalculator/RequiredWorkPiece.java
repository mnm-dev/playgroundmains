package at.mnm.cuttingcalculator;

import java.io.PrintStream;
import java.util.Deque;
import java.util.LinkedList;

public class RequiredWorkPiece implements HasIdentity<RequiredWorkPiece>, Comparable<RequiredWorkPiece> {

   public final Identity id;
   public final int requiredDimension;
   public int leftToCut;
   public final Deque<Integer> parts;

   public RequiredWorkPiece(int requiredDimension) {
      this(new Identity(), requiredDimension, requiredDimension, new LinkedList<>());
   }

   private RequiredWorkPiece(Identity id, int requiredDimension, int leftToCut, Deque<Integer> parts) {
      this.id = id;
      this.requiredDimension = requiredDimension;
      this.leftToCut = leftToCut;
      this.parts = new LinkedList<>(parts);
   }

   public RequiredWorkPiece clone() {
      return new RequiredWorkPiece(id, requiredDimension, leftToCut, parts);
   }

   public void cut(int cutDimension) {

      if (CuttingSettings.SAFE_MODE && (cutDimension > leftToCut || cutDimension <= 0)) {
         throw new IllegalStateException("Bogus cut: cutDimension: " + cutDimension + " requiredDimension: "
               + requiredDimension + " leftOver: " + leftToCut);
      }

      leftToCut -= cutDimension;
      parts.push(cutDimension);
   }

   public void revertLastCut() {
      Integer cutDimension = parts.pop();

      if (CuttingSettings.SAFE_MODE && ((cutDimension + leftToCut) > requiredDimension || cutDimension <= 0)) {
         throw new IllegalStateException("Bogus revert: cutDimension: " + cutDimension + " requiredDimension: "
               + requiredDimension + " leftOver: " + leftToCut);
      }

      leftToCut += cutDimension;
   }

   public boolean isComplete() {
      return leftToCut == 0;
   }

   public void output(PrintStream out) {
      out.println("    requiredDimension : " + requiredDimension + ", leftToCut : " + leftToCut + ", parts : " + parts);
   }

   @Override
   public Identity getId() {
      return id;
   }

   @Override
   public int compareTo(RequiredWorkPiece other) {
      return other.requiredDimension - this.requiredDimension;
   }

   @Override
   public String toString() {
      return super.toString() + " [id=" + id + ", requiredDimension=" + requiredDimension + ", leftToCut=" + leftToCut
            + ", parts=" + parts + "]";
   }

   @Override
   public int hashCode() {
      final int prime = 31;
      int result = 1;
      result = prime * result + leftToCut;
      result = prime * result + ((parts == null) ? 0 : parts.hashCode());
      result = prime * result + requiredDimension;
      return result;
   }

   @Override
   public boolean equals(Object obj) {
      if (this == obj)
         return true;
      if (obj == null)
         return false;
      if (getClass() != obj.getClass())
         return false;
      RequiredWorkPiece other = (RequiredWorkPiece) obj;
      if (leftToCut != other.leftToCut)
         return false;
      if (parts == null) {
         if (other.parts != null)
            return false;
      } else if (!parts.equals(other.parts))
         return false;
      if (requiredDimension != other.requiredDimension)
         return false;
      return true;
   }

}
