package at.mnm.cuttingcalculator;

public class CuttingSettingsBuilder {

   private int minimumRequiredSize;
   private int sawBladeLossLength;
   private int maxSectionsPerRequiredSize;
   private boolean needCleanEnds;
   private int someAbritraryPiece;
   private int safetyMargin;
   private boolean allowArbitraryCuts = true;

   /**
    * if boards are not of exact sizes or our blade cut offs are too large, then a
    * solution might not work one option is to increase the saw blade thickness to
    * something wider, or o get warnings if the solution might be cutting it close
    * by setting a safetyMargin (this will only affect the output of warnings, it
    * won't affect the solutions.)
    * 
    * @param safetyMargin
    * @return
    */
   public CuttingSettingsBuilder withSafetyMargin(int safetyMargin) {
      this.safetyMargin = safetyMargin;
      return this;
   }

   public CuttingSettingsBuilder withSawBladeLossLength(int sawBladeLossLength) {
      if (sawBladeLossLength < 0) {
         throw new IllegalArgumentException("Saw blade loss cant be negative");
      }
      this.sawBladeLossLength = sawBladeLossLength;
      return this;
   }

   public CuttingSettingsBuilder maxSectionsPerRequiredLength(int maxSectionsPerRequiredLength) {
      this.maxSectionsPerRequiredSize = maxSectionsPerRequiredLength;
      return this;
   }

   public CuttingSettingsBuilder withMinimumRequiredSize(int minimumRequiredSize) {
      this.minimumRequiredSize = minimumRequiredSize;
      this.someAbritraryPiece = (int) (minimumRequiredSize * CuttingSettings.SOME_CUTOFF_FACTOR);
      return this;
   }

   public CuttingSettingsBuilder withCleanEnds() {
      this.needCleanEnds = true;
      return this;
   }

   public CuttingSettingsBuilder withoutArbitraryCuts() {
      this.allowArbitraryCuts = true;
      return this;
   }

   public CuttingSettings build() {
      
      if (!allowArbitraryCuts) {
         this.someAbritraryPiece = 0;
      }
      
      return new CuttingSettings(
            minimumRequiredSize, sawBladeLossLength, maxSectionsPerRequiredSize, needCleanEnds, someAbritraryPiece,
            safetyMargin);
   }

}
