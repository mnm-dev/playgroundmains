package at.mnm.cuttingcalculator;

import java.util.concurrent.atomic.AtomicBoolean;

public class RunState {
   AtomicBoolean quitting = new AtomicBoolean(false);
   AtomicBoolean complete = new AtomicBoolean(false);
   public int depth;
}
