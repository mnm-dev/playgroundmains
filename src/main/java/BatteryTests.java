import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.time.Period;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.opencsv.CSVReader;

/*
 *
 *

 2, 4, 6, 8, 10 battery show loading level
 Summarize per day use and generation, show min/max battery level, load level at times: before startOf Gen and at end of Gen

 how much energy stored and pulled  from battery total and min/max

 how much solar min/avg/max per month

 Calculate for each battery size the amount of energy
 exported to and pulled from grid, preferable with peak/offpeak/shoulder separated out

 */

public class BatteryTests {

    private static final boolean skipWinter = false;

    int[] sizes = { 0, 2_000, 4_000, 6_000, 8_000, 10_000, 20_000, 40_000, 80_000, 160_000, 2_020_000 };

    Map<Integer, ElectroStats> totalStats = new HashMap<>();
    Map<Integer, ElectroStats> monthlyStats = new HashMap<>();
    Map<Integer, ElectroStats> weeklyStats = new HashMap<>();
    Map<Integer, ElectroStats> dailyStats = new HashMap<>();
    public int rowCount = 0;

    private void processFile(File file) throws FileNotFoundException, IOException {

        List<String[]> allRows = null;
        try (CSVReader reader = new CSVReader(new FileReader(file))) {
            allRows = reader.readAll();
        }

        totalStats = initialiseStats(sizes, Period.ofYears(100));

        boolean firstRow = true;
        for (String[] row : allRows) {
            rowCount++;
            if (firstRow) {
                firstRow = false;
                continue;
            }

            LocalDateTime rowDate = LocalDateTime.parse(row[0].replaceAll(" ", "T"));
            long used = wattsFromKilowattString(row[1]);
            long generated = wattsFromKilowattString(row[2]);
            if (isSkipwinter() && isWinter(rowDate)) {
                continue;
            }

            CostClass currentCostClass = getCostClassForDate(rowDate);

            for (int size : sizes) {
                ElectroStats electroStats = totalStats.get(size);
                electroStats.adjustStats(rowDate, currentCostClass, generated, used);

            }

        }

        System.out.println("output");
        for (int size : sizes) {
            ElectroStats electroStats = totalStats.get(size);
            System.out.println(electroStats.toString());
        }

        System.out.println("");
        System.out.println(totalStats.get(sizes[0]).toCsvHeader());
        for (int size : sizes) {
            ElectroStats electroStats = totalStats.get(size);
            System.out.println(electroStats.toCsvRow());
        }

    }

    private boolean isWinter(LocalDateTime rowDate) {
        return rowDate.getMonthValue() >= 6 && rowDate.getMonthValue() <= 8;
    }

    public static class ElectroStats {

        public final Period period;
        public final long batterySize;
        public LocalDateTime periodStart;

        public long totalGridUsePeak;
        public long totalGridUseShoulder;
        public long totalGridUseOffPeak;
        public long totalUsed;
        public long totalGenerated;
        public long totalExported;

        public long minCharged;
        public long maxCharged;
        public long currentCharge;

        public ElectroStats(long batterySize, Period period) {
            this.batterySize = batterySize;
            this.period = period;
        }

        public void adjustStats(LocalDateTime rowDate, CostClass currentCostClass, long generated, long used) {

            if (used == 0 && generated == 0)
                return;

            totalGenerated += generated;
            totalUsed += used;

            long newCharge = currentCharge + generated - used;

            if (newCharge < 0) {
                long usedFromGrid = -1 * newCharge;
                increaseGridUse(currentCostClass, usedFromGrid);
                currentCharge = 0;
            } else if (newCharge > batterySize) {
                totalExported += (newCharge - batterySize);
                currentCharge = batterySize;
            } else {
                currentCharge = newCharge;
            }

            minCharged = Math.min(minCharged, currentCharge);
            maxCharged = Math.max(maxCharged, currentCharge);
        }

        public void increaseGridUse(CostClass currentCostClass, long gridUse) {
            switch (currentCostClass) {
            case PEAK:
                totalGridUsePeak += gridUse;
                break;
            case SHOULDER:
                totalGridUseShoulder += gridUse;
                break;
            case OFFPEAK:
                totalGridUseOffPeak += gridUse;
                break;

            default:
                break;
            }

        }

        @Override
        public String toString() {
            return "ElectroStats [period=" + period + ", batterySize=" + batterySize + ", periodStart=" + periodStart
                    + ", totalGridUse=" + (totalGridUsePeak + totalGridUseShoulder + totalGridUseOffPeak)
                    + ", totalGridUsePeak=" + totalGridUsePeak + ", totalGridUseShoulder=" + totalGridUseShoulder
                    + ", totalGridUseOffPeak=" + totalGridUseOffPeak + ", totalUsed=" + totalUsed + ", totalGenerated="
                    + totalGenerated + ", totalExported=" + totalExported + ", minCharged=" + minCharged
                    + ", maxCharged=" + maxCharged + ", currentCharge=" + currentCharge + "]";
        }

        public String toCsvHeader() {
            return "batterySize,totalGridUse, totalGridUsePeak,totalGridUseShoulder,totalGridUseOffPeak,totalUsed,totalGenerated,totalExported";
        }

        public String toCsvRow() {
            return String.format("%d,%d,%d,%d,%d,%d,%d,%d", batterySize / 1000,
                    (totalGridUsePeak + totalGridUseShoulder + totalGridUseOffPeak) / 1000, totalGridUsePeak / 1000,
                    totalGridUseShoulder / 1000, totalGridUseOffPeak / 1000, totalUsed / 1000, totalGenerated / 1000,
                    totalExported / 1000);
        }

    }

    private Map<Integer, ElectroStats> initialiseStats(int[] sizes, Period period) {

        Map<Integer, ElectroStats> stats = new HashMap<>();

        for (int size : sizes) {
            stats.put(size, new ElectroStats(size, period));
        }

        return stats;
    }

    public CostClass getCostClassForDate(LocalDateTime rowDate) {

        if (rowDate.getDayOfWeek() == DayOfWeek.SATURDAY || rowDate.getDayOfWeek() == DayOfWeek.SUNDAY) {
            if (rowDate.getHour() >= 22 || rowDate.getHour() < 7) {
                return CostClass.OFFPEAK;
            } else {
                return CostClass.SHOULDER;
            }
        } else {
            if (rowDate.getHour() >= 22 || rowDate.getHour() < 7) {
                return CostClass.OFFPEAK;
            } else if (rowDate.getHour() >= 20 || rowDate.getHour() < 14) {
                return CostClass.SHOULDER;
            } else {
                return CostClass.PEAK;
            }

        }
    }

    private long wattsFromKilowattString(String kiloWattString) {
        Double w = Double.valueOf(kiloWattString) * 1000;
        return w.longValue();
    }

    public static enum CostClass {

        PEAK, SHOULDER, OFFPEAK

    }

    
    
    
    public static void main(String[] args) throws FileNotFoundException, IOException {

        String filepath = args[0];

        BatteryTests batteryTests = new BatteryTests();
        try {
            batteryTests.processFile(new File(filepath));
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(batteryTests.rowCount);
        }

    }

   public static boolean isSkipwinter() {
      return skipWinter;
   }

}
